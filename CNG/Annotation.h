//
//  Annotation.h
//  CNG
//
//  Created by Petr Jelínek on 04/05/14.
//  Copyright (c) 2014 Petr Jelinek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface Annotation : NSObject <MKAnnotation>

@property(nonatomic, assign) CLLocationCoordinate2D coordinate;
@property(nonatomic, copy) NSString * title;
@property(nonatomic, copy) NSString * subtitle;


@end
