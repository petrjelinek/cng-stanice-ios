//
//  AppDelegate.h
//  CNG
//
//  Created by Petr Jelinek on 28/11/13.
//  Copyright (c) 2013 Petr Jelinek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
