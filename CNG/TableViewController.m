//
//  TableViewController.m
//  CNG
//
//  Created by Petr Jelínek on 06/05/14.
//  Copyright (c) 2014 Petr Jelinek. All rights reserved.
//

#import "TableViewController.h"
#import "MapViewController.h"
#import "DetailViewController.h"

@interface TableViewController ()

@end

@implementation TableViewController
@synthesize myTableView, seznamStanic;

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.backgroundColor = [UIColor groupTableViewBackgroundColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Station *stanice = [seznamStanic objectAtIndex:indexPath.row];
    selectedDetailId = stanice.cislo;
    //NSLog(@"STANICE CISLO:  %lo", stanice.cislo);
    DetailViewController *detail = (DetailViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
    detail.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:detail animated:YES completion:nil];
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.myTableView.delegate = self;
 //   self.myTableView.dataSource = self;
    lastView = 2;
    MapViewController * mvc = [[MapViewController alloc] init];
    seznamStanic = [[NSMutableArray alloc] init];
    seznamStanic = [mvc getStations];
    //NSLog(@"SEZNAM STANIC COUNT:   %lo", seznamStanic.count);
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [seznamStanic count];
   // return 0;
   
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellId = @"Cell ID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    
    // Configure the cell...
    Station *stanice = [seznamStanic objectAtIndex:indexPath.row];
    //NSLog(@"STANICE LABEL:  %@" , stanice.title);
    cell.textLabel.text = stanice.subtitle;
    
    return cell;
}

- (IBAction)backButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
