//
//  OblibeneViewController.h
//  CNG
//
//  Created by Petr Jelínek on 06/05/14.
//  Copyright (c) 2014 Petr Jelinek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic, strong) NSMutableArray *seznamOblibenychStanic;

- (IBAction)backButtonPressed:(id)sender;

@end
