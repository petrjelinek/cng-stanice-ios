//
//  TableViewController.h
//  CNG
//
//  Created by Petr Jelínek on 06/05/14.
//  Copyright (c) 2014 Petr Jelinek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (nonatomic, strong) NSMutableArray *seznamStanic;
- (IBAction)backButtonPressed:(id)sender;

@end
