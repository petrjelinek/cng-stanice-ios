//
//  ViewController.m
//  CNG
//
//  Created by Petr Jelinek on 28/11/13.
//  Copyright (c) 2013 Petr Jelinek. All rights reserved.
//

#import "ViewController.h"

#import "Station.h"
#define METERS_PER_MILE 1609.344

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    
// zjistí severovýchodní a jihozápadní body úvodní obrazovky a uloží je pro další použití
    CGPoint nePoint = CGPointMake(_mapView.bounds.origin.x + _mapView.bounds.size.width, _mapView.bounds.origin.y);
    CGPoint swPoint = CGPointMake(_mapView.bounds.origin.x, _mapView.bounds.origin.y + _mapView.bounds.size.height);
    
    CLLocationCoordinate2D neCoord;
    neCoord = [_mapView convertPoint:nePoint toCoordinateFromView:_mapView];
    CLLocationCoordinate2D swCoord;
    swCoord = [_mapView convertPoint:swPoint toCoordinateFromView:_mapView];
    
    [super viewDidLoad];
 
    
    Station * station1 = [Station alloc];
    CLLocationCoordinate2D coord;
    coord.latitude = 50.177474;
    coord.longitude = 14.671795;
    station1 = [self createStationWith:@"BONETT EUROGAS CNG" withSubtitle:@"Brandýs nad Labem" withCislo:@"1" withAdresa:@"Zápská 1855, Brandýs nad Labem" withOteviraciDoba:@"NONSTOP" withProvozovatel:@"Bonett Gas Investment, a.s." withKontakt:@"+420 737 252 767, obchod@bonett.cz" withZpusobPlneni:@"samoobslužné, stojan 2 x NGV 1" withZpusobPlaceni:@"karta CNG CardCentrum, karta Bonett Eurogas, CCS, EC/MC, Visa, hotovost" withCoordinate:coord];
    [self.mapView addAnnotation:station1];
    
    UIPinchGestureRecognizer* recognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handlePinch:)];
    
    [self.mapView addGestureRecognizer:recognizer];
    
}

- (Station *)createStationWith:(NSString*)title
             withSubtitle:(NSString*)subtitle
                withCislo:(NSString*)cislo
               withAdresa:(NSString*)adresa
        withOteviraciDoba:(NSString*)oteviraciDoba
         withProvozovatel:(NSString*)provozovatel
              withKontakt:(NSString*)kontakt
         withZpusobPlneni:(NSString*)zpusobPlneni
        withZpusobPlaceni:(NSString*)zpusobPlaceni
           withCoordinate:(CLLocationCoordinate2D)coordinate
{
    Station * station = [Station alloc];
    station.coordinate = coordinate;
    station.title = title;
    station.subtitle = subtitle;
    station.cislo = cislo;
    station.adresa = adresa;
    station.oteviraciDoba = oteviraciDoba;
    station.provozovatel = provozovatel;
    station.kontakt = kontakt;
    station.zpusobPlaceni = zpusobPlaceni;
    station.zpusobPlneni = zpusobPlneni;
    
    return station;
    
}

// maximální zoom out na velikost ČR
- (void)handlePinch:(UIPinchGestureRecognizer*)recognizer
{
    static MKCoordinateRegion originalRegion;
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        originalRegion = self.mapView.region;
    }
    
    double latdelta = originalRegion.span.latitudeDelta / recognizer.scale;
    double londelta = originalRegion.span.longitudeDelta / recognizer.scale;
    
    // TODO: set these constants to appropriate values to set max/min zoomscale
    latdelta = MAX(MIN(latdelta, 6.88), 0.03);
    londelta = MAX(MIN(londelta, 6.88), 0.03);
    MKCoordinateSpan span = MKCoordinateSpanMake(latdelta, londelta);
    
    [self.mapView setRegion:MKCoordinateRegionMake(originalRegion.center, span) animated:YES];
}

////////////////
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    static NSString *identifier = @"MyLocation";
    if ([annotation isKindOfClass:[Station class]]) {
        
        MKPinAnnotationView *annotationView =
        (MKPinAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (annotationView == nil) {
            annotationView = [[MKPinAnnotationView alloc]
                              initWithAnnotation:annotation
                              reuseIdentifier:identifier];
        } else {
            annotationView.annotation = annotation;
        }
        
        annotationView.enabled = YES;
        annotationView.canShowCallout = YES;
        
        // Create a UIButton object to add on the
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [rightButton setTitle:annotation.title forState:UIControlStateNormal];
        [annotationView setRightCalloutAccessoryView:rightButton];
        
        UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeInfoDark];
        [leftButton setTitle:annotation.title forState:UIControlStateNormal];
        [annotationView setLeftCalloutAccessoryView:leftButton];
        
        return annotationView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView
 annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
    if ([(UIButton*)control buttonType] == UIButtonTypeDetailDisclosure){
        // Do your thing when the detailDisclosureButton is touched
        UIViewController *mapDetailViewController = [[UIViewController alloc] init];
        [[self navigationController] pushViewController:mapDetailViewController animated:YES];
        
    } else if([(UIButton*)control buttonType] == UIButtonTypeInfoDark) {
        // Do your thing when the infoDarkButton is touched
        
        NSLog(@"infoDarkButton for longitude: %f and latitude: %f",
              [(Station*)[view annotation] coordinate].longitude,
              [(Station*)[view annotation] coordinate].latitude);
    }
}
////////////////////

// nastaví zpátky na původní region - ČR
- (void)setDefaultRegion:(MKCoordinateRegion)region animated:(BOOL)animated
{
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 49.6;
    zoomLocation.longitude= 15.5;
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 498896.64, 498896.64);
    [_mapView setRegion:viewRegion animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    // 1
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 49.6;
    zoomLocation.longitude= 15.5;
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 498896.64, 498896.64);
    
    // 3
    [_mapView setRegion:viewRegion animated:YES];
}

@end
