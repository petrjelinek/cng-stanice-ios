//
//  DetailViewController.h
//  CNG
//
//  Created by Petr Jelínek on 05/05/14.
//  Copyright (c) 2014 Petr Jelinek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Station.h"

@interface DetailViewController : UIViewController <UIAlertViewDelegate>
- (IBAction)navigateButtonPressed:(id)sender;
- (IBAction)addButtonPressed:(id)sender;
- (IBAction)backButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *titleLable;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLable;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *openHours;
@property (weak, nonatomic) IBOutlet UILabel *owner;
@property (weak, nonatomic) IBOutlet UILabel *contact;
@property (weak, nonatomic) IBOutlet UILabel *filling;
@property (weak, nonatomic) IBOutlet UILabel *payment;

@property (nonatomic, strong)Station* station;

@end
